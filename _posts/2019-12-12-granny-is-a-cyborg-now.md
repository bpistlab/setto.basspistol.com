---
title: Granny is a cyborg now
author: setto
date: 2019-12-12 04:03:00 +0100
description: Jazzy Cybersoul with a hint of 303 baselines.
image: /images/posts/1-grandma-got-the-ssh-keys.jpeg
video_path: /videos/37th--chamber.mp4
category: releases
tags:
  - EP
link:
  url:
  buttontext:
check_this_if_cannot_be_bought: false
album: cyber-grany
stores:
  - name: Audius
    url: 'https://audius.co/tosettosetto/grandma-is-a-cyborg-now-92454'
    icon: fa-creative-commons-sampling
    download: false
---

My grandmother, 93,&nbsp; added me on social media yesterday. So I called her. Long-chat good talk. TL;DR Damn what a woman\! She told me she had a roll-walker. I said "we're in the future now Grandma, you're a cyborg, half machine\!"

She laughed and acknowledge it was a marvellous invention\!

Now with vocals on Audius\!

<iframe sandbox="allow-same-origin allow-scripts allow-popups" src="https://v.basspistol.org/videos/embed/0073ff9d-d255-40a3-80de-fb3d37bf9ab2" allowfullscreen="" width="100%" height="315" frameborder="0"></iframe>

&nbsp;