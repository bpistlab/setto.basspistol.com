---
title: Social Media Distancing
author: setto
date: 2020-11-17 16:20:00 +0100
description: 'We see each other less, but when we do it''s on the same screen we work on.'
image: /images/posts/digital-detox.jpg
video_path:
category: ramblings
tags:
link:
  url: 'https://v.basspistol.org/accounts/setto/video-channels'
  buttontext: "Subscribe to my PeerTube \U0001F4FA"
check_this_if_cannot_be_bought: true
album:
stores:
  - name:
    url:
    icon:
    download: false
---

The mainstream networks have gone void of actual content. Everybody is either mimicking something or avoiding any sort of relevance because "it just creates conflicts". And maybe that makes sense in these dark times? Remote work has got us so stuck to our screens that we'd rather not stick around on there. The escapism escaped from those places when the corporation jumped in.

I really do my best to social-media distance myself from the usual suspects. And there are plenty of alternatives:

* [Pixelfed](https://pixfed.com/tosettosetto){: target="_blank"} replaces Insta
* [Mastodon](https://kolektiva.social/@setto){: target="_blank"} replaces Twitter
* [Matrix](https://matrix.to/#/!YQcdVviFQNGYFMYrxD:matrix.org?via=matrix.org){: target="_blank"} replaces Whatsapp
* [PeerTube](https://v.basspistol.org) replace YouTube

The fediverse is a diverse and federate network. Here you can start afresh. Be that GeoCities person you used to be? Or simply a lurker\! It becomes what we make it… The cool thing about Fediverse is that once you have an identity on one of those systems, say mastodon, you can interact with content from all systems\! Be it pixelfed, peertube, disapora…

Try it out\! I suggest you start by looking for a [community node that fits your vibe.](https://joinmastodon.org/communities){: target="_blank"}

> Mastodon isn’t a single website like Twitter or Facebook, it's a network of thousands of communities operated by different organizations and individuals that provide a seamless social media experience.

Get your self an identity on there and once logged in, search for "@setto@v.basspistol.org" That is my PeerTube identity. Wonder how? Ask [me on matrix!](/chat/#read) :)