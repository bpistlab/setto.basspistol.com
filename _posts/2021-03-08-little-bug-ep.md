---
title: Little Bug EP
author: setto
date: 2021-03-08 19:07:00 +0100
description: Lofi Hiphop from planet Foss.
image: /images/posts/littlebug1400x1400.png
video_path:
category: releases
tags:
  - EP
link:
  url:
  buttontext:
check_this_if_cannot_be_bought: false
album: little-bug-ep
download_form_id: 11
stores:
  - name: Bandcamp
    url: 'https://shop.basspistol.com/album/little-bug-ep'
    icon: fa-bandcamp
    download: true
  - name: Audius
    url: 'https://audius.co/tosettosetto/playlist/little-bug-ep-31143'
    icon: fa-creative-commons-sampling
    download: false
---
A smol EP for human kind. A giant leap for me. This is my first release that is entirely produced using Cooperative software (AKA FLOSS). As in: no hardware\!\! Yes\! it is also my first release in over a decade where all the synths and drum machines are software modules. It features flute and sax from the talented [Randulo](https://indieweb.social/@randulo){: target="_blank" rel="noopener noreferrer"}, and I am happy to share it with you.

<iframe title="Little Bug - 徒 setto セット" src="https://v.basspistol.org/videos/embed/82b6d298-be44-4c64-9941-b66d09b2723b" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="100%" height="315" frameborder="0"></iframe>

Let me know what you think\! You can talk to me [here](/chat/#read)

&nbsp;
