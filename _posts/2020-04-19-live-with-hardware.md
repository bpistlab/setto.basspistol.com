---
title: Live with hardware
author: setto
date: 2020-04-19 09:07:00 +0200
description: >-
  There is a weird meta-core to anything social exchange on internet that I
  can't get my finger on. Assumptions and projections that I think partially
  stem from the lack in body language
image: /images/posts/1-ep-life-of-a-dudelini.jpeg
video_path:
category: releases
tags:
  - Mixtape
link:
  url:
  buttontext:
album: live-with-hardware
check_this_if_cannot_be_bought: true
stores:
  - name:
    url:
    icon:
    download: false
---

There is a weird meta-core to anything social exchange on internet that I can't get my finger on. Assumptions and projections that I think partially stem from the lack in body language and the fact that we tend to be alone with our devices when we exchange. Even when we do so while our physical self is in a room full of friends. One of it's symptoms is the *endorsement paradox,* for lack of better words. At some point it was common to see Twitter bios with the line *"retweets \!= Endorsement".* It might have a better name, but that "endorsement paradox" is something I feel heavily used as a propaganda strategy. The thing is, there is always someone in anyone's acquaintances that will resonate to shared info and critique in unexpected ways. For example some unspeakable parties triggered everyone to a point where everyone got so obsessed that everyone kept talking about those parties non stop. Basically taking said unspeakable parties out of the void they were in, and promoting them into a mass that had to be accounted for. I'm not saying unspeakable party propaganda should be ignored, but I think some information is better dissected away from public spaces where there is no control over the dimension of time since the discussion can keep on forever, even when everyone involved at first have moved on. Like that which happened to 4chan.

It takes at least 2 to have a dialogue. And to achieve that plurality in cyberspace seems to require a lot of effort. Probably due to that aforementioned conflict between solitude and time-traversing company. "Company" as in being accompanied and time-traversing because what we record today can be used in the future, and in the future we might use recordings from the past.

Either way, there is always something that escapes me when I try to thinker about exchange of information that pertain to the power limbo between humans. Hence the idea of a meta core: like a Russian doll. After all information is vehicular to energy. Be it human or robot information exchange: an impulse that turns a turbine on, a poem that make your heart beat faster…

Audio extracted from the 360 video of [this jam](/hw-jam/) recorded a while back.

<iframe sandbox="allow-same-origin allow-scripts allow-popups" src="https://v.basspistol.org/videos/embed/a6e9af57-b650-4274-92a8-5e806e641420" allowfullscreen="" width="100%" height="360" frameborder="0"></iframe>