---
title: Unmet Dependencies
author: setto
date: 2020-08-02 07:00:00
description: >-
  Proprietary tools, consultancies, remote labor... The reality in the digital
  industry is as hard to cope with as it is building software in Haskell.
  Freedom is luxury, but also a burden.


  Open your mind, open the source!
image: /images/posts/final-pingnucover1220x1200.jpg
video_path: /videos/pingu.mp4
category: releases
tags:
  - EP
link: 
  url: 
  buttontext: 
check_this_if_cannot_be_bought: false
album: unmet-dependencies
stores:
  - name: Bandcamp
    url: 'https://shop.basspistol.com/album/unmet-dependencies'
    icon: fa-bandcamp
    download: true
  - name: Audius
    url: 'https://audius.co/tosettosetto/playlist/unmet-dependencies-11206'
    icon: fa-creative-commons-sampling
    download: false
  - name: PeerTube
    url: >-
      https://v.basspistol.org/videos/watch/playlist/e2e57ea1-5ed1-41a2-afd9-f8f9a0b7d1c6
    icon: fa-youtube
    download: false
---

## Oldies, but goldies

It is through these 2 tracks that i found the To Setto Setto in me. Before that i used to do some (Excellent, but) sort of lamo Electro-pop. Originally released under Sethybwoy, around 2016, they got some traction in Cyperpunk and geek circles, but never quite made it further…

I don't really expect them to go so much longer this time, the odds are about the same as winning the EU-Jackpot Lottery. But i decided they belonged to To Setto Setto now and i can do as I please with them.

Hope you like them\!

Here is a video that was made for the track Pingnu. It's cute AF… but ok, that's how it was made\!

<iframe sandbox="allow-same-origin allow-scripts allow-popups" src="https://v.basspistol.org/videos/embed/eee983fc-6619-4804-8e49-1b276015f8fa?subtitle=fr" allowfullscreen="" width="100%" height="360" frameborder="0"></iframe>