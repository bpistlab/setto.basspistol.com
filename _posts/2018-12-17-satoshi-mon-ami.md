---
title: Satoshi Mon Ami
author: setto
date: 2018-12-17 19:00:00
description: >-
  The 2 tracks of laid back cloud rap are tales of daily digital life-struggles.
  When day-to-day communication blurs the line between the virtual and real.
image: /images/posts/1-k-as-tu-fait-de-nous-.jpeg
video_path: /videos/satoshi.webm
category: releases
tags:
  - EP
link: 
  url: 
  buttontext: 
check_this_if_cannot_be_bought: false
album: satoshi-mon-ami
stores:
  - name: Bandcamp
    url: 'https://shop.basspistol.com/album/satoshi-mon-ami'
    icon: fa-bandcamp
    download: true
  - name: Audius
    url: 'https://audius.co/tosettosetto/playlist/satoshi-mon-ami-11205'
    icon: fa-creative-commons-sampling
  - name: Peertube
    url: >-
      https://v.basspistol.org/videos/watch/playlist/262e54c5-6f15-402a-972d-474a898129df
    icon: fa-youtube
    download: false
---

The 2 tracks of laid back cloud rap are tales of daily digital life-struggles. When day-to-day communication blurs the line between the virtual and real.

Being close to someone, without them knowing, anonymously and obsessively through a device. Decrypting interconnected sub-stories from every node in the graph and losing your mind on it.What's the latest story? FOMO to the max, The Fear Of Missing Out\!

Put your derpy VR-goggles on and check out the 360 video bellow\!

<iframe src="https://www.youtube-nocookie.com/embed/mOPubF0xhG8" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="100%" height="360" frameborder="0"></iframe>

The video is delivered from YouTube, but is explicitly set not to push any cookies to your browser\! [Click here for a direct link](https://www.youtube.com/watch?v=mOPubF0xhG8){: target="_blank"}