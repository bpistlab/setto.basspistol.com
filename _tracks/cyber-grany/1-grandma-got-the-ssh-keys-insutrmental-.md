---
layout: track
path: >-
  /mnt/usb32gb/organized-setto.media/assets/albums/cyber-grany/1-grandma-got-the-ssh-keys-insutrmental-.mp3
audio: /assets/albums/cyber-grany/1-grandma-got-the-ssh-keys-insutrmental-.mp3
slug: cyber-grany/1-grandma-got-the-ssh-keys-insutrmental-
albumSlug: cyber-grany
trackSlug: 1-grandma-got-the-ssh-keys-insutrmental-
coverPath: >-
  /mnt/usb32gb/organized-setto.media/assets/albums/cyber-grany/1-grandma-got-the-ssh-keys-insutrmental-.jpeg
cover: /assets/albums/cyber-grany/1-grandma-got-the-ssh-keys-insutrmental-.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 8910720
  duration: 202.05714285714285
native:
  ID3v2.3:
    - id: TIT2
      value: Grandma got the SSH keys (Insutrmental)
    - id: TPE1
      value: 徒 Setto セット
    - id: TRCK
      value: 1/2
    - id: TALB
      value: Cyber Grany
    - id: TPE2
      value: To Setto Setto
    - id: TCON
      value: Acid Trap
    - id: 'TXXX:CDDB DiscID'
      value: 0200ca01
    - id: 'TXXX:discid'
      value: 0200ca01
    - id: 'TXXX:MusicBrainz DiscID'
      value: bhKd3SFqkr6x1L9X2JkIZ6UIaRA-
    - id: 'TXXX:musicbrainz_discid'
      value: bhKd3SFqkr6x1L9X2JkIZ6UIaRA-
    - id: COMM
      value:
        language: xxx
        description: WEBSITE
        text: 'https://setto.basspistol.com'
    - id: COMM
      value:
        language: xxx
        description: Comment
        text: Special track for Acid December 2019
    - id: TYER
      value: '2019'
quality:
  warnings: []
common:
  track:
    'no': 1
    of: 2
  disk:
    'no': null
    of: null
  title: Grandma got the SSH keys (Insutrmental)
  artists:
    - 徒 Setto セット
  artist: 徒 Setto セット
  album: Cyber Grany
  albumartist: To Setto Setto
  genre:
    - Acid Trap
  comment:
    - 'https://setto.basspistol.com'
    - Special track for Acid December 2019
  year: 2019
transformed:
  ID3v2.3:
    TIT2: Grandma got the SSH keys (Insutrmental)
    TPE1: 徒 Setto セット
    TRCK: 1/2
    TALB: Cyber Grany
    TPE2: To Setto Setto
    TCON: Acid Trap
    'TXXX:CDDB DiscID': 0200ca01
    'TXXX:discid': 0200ca01
    'TXXX:MusicBrainz DiscID': bhKd3SFqkr6x1L9X2JkIZ6UIaRA-
    'TXXX:musicbrainz_discid': bhKd3SFqkr6x1L9X2JkIZ6UIaRA-
    WEBSITE: 'https://setto.basspistol.com'
    Comment: Special track for Acid December 2019
    TYER: '2019'
all:
  TIT2: Grandma got the SSH keys (Insutrmental)
  TPE1: 徒 Setto セット
  TRCK: 1/2
  TALB: Cyber Grany
  TPE2: To Setto Setto
  TCON: Acid Trap
  'TXXX:CDDB DiscID': 0200ca01
  'TXXX:discid': 0200ca01
  'TXXX:MusicBrainz DiscID': bhKd3SFqkr6x1L9X2JkIZ6UIaRA-
  'TXXX:musicbrainz_discid': bhKd3SFqkr6x1L9X2JkIZ6UIaRA-
  WEBSITE: 'https://setto.basspistol.com'
  Comment: Special track for Acid December 2019
  TYER: '2019'
nextTrack: &ref_1
  path: >-
    /mnt/usb32gb/organized-setto.media/assets/albums/cyber-grany/2-grand-ma-is-a-cyborg-now.mp3
  audio: /assets/albums/cyber-grany/2-grand-ma-is-a-cyborg-now.mp3
  slug: cyber-grany/2-grand-ma-is-a-cyborg-now
  albumSlug: cyber-grany
  trackSlug: 2-grand-ma-is-a-cyborg-now
  coverPath: >-
    /mnt/usb32gb/organized-setto.media/assets/albums/cyber-grany/2-grand-ma-is-a-cyborg-now.jpeg
  cover: /assets/albums/cyber-grany/2-grand-ma-is-a-cyborg-now.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 48000
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 9568512
    duration: 199.344
  native:
    ID3v2.3:
      - id: TIT2
        value: Grand'ma Is A Cyborg Now
      - id: TPE1
        value: 徒 setto セット
      - id: TALB
        value: Cyber Grany
      - id: TRCK
        value: 2/2
      - id: TCON
        value: Acid Trap
      - id: TPE2
        value: To Setto Setto
      - id: USLT
        value: &ref_0
          language: eng
          description: ''
          text: |-
            Got a friend request from my grandma
            But she spelled her name with an O
            Felt like it came from Futurama
            but I know my OP-sec yo!

            Background check just didn't add up
            Family tree is kind of messy
            So I called her up
            She loled out loud and said: hey sweetie!

            Why is it that you never call me?
            Good thing I went through the zuck
            Although I think it's kind of shitty
            I simply had to try my luck

            Do I know what it does to democracy?
            How come we have to be registered?
            How can this promote any liberty?
            If all we do in it is monitored?

            So many questions and all valid,
            She checked all my OP-sec control
            94 years old and vivid
            Holding strong her grandma role.

            She said next time we go for a stroll
            I'll get to see her augmentation:
            A 2kg aluminum walker-roll
            A marvelous invention!
      - id: TYER
        value: '2020'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: 2
    disk:
      'no': null
      of: null
    title: Grand'ma Is A Cyborg Now
    artists:
      - 徒 setto セット
    artist: 徒 setto セット
    album: Cyber Grany
    genre:
      - Acid Trap
    albumartist: To Setto Setto
    year: 2020
  transformed:
    ID3v2.3:
      TIT2: Grand'ma Is A Cyborg Now
      TPE1: 徒 setto セット
      TALB: Cyber Grany
      TRCK: 2/2
      TCON: Acid Trap
      TPE2: To Setto Setto
      USLT: *ref_0
      TYER: '2020'
  all:
    TIT2: Grand'ma Is A Cyborg Now
    TPE1: 徒 setto セット
    TALB: Cyber Grany
    TRCK: 2/2
    TCON: Acid Trap
    TPE2: To Setto Setto
    USLT: *ref_0
    TYER: '2020'
previousTrack: *ref_1
---
