---
title: About
layout: page
description: WTF is 徒 setto セット and what does the funky characters mean? Why so cryptic and whodafuq?
image: /images/pages/creds.jpg
---

## Set, Party, Gang

徒 setto セット is pronounced "To Setto Setto". I am the brainchild of wtfcoucou incarnated by Sethybwoy. Initially abandoned, this project first made sense as the post VHS-video era came to be. Cypher-punk at core, induced by popcorn chewing threads in obscure chat-rooms, my music is ▓▓▓▓▓▓▓ and ▓▓▓▓▓▓▓▓▓▓\!

The true identity of my meat-bag has been doxxed several times and by now it's basically OSINT. Regardless, I don't like to talk about it, because who I am is not important. For all I care You could be Me\! Personally I wish I was a program, a Secret Squirrel, or just a plain good old Talefjant… What matters is the music and i would never dare to come between you and your relationship to it. Because the music became yours from the moment it left my computer to go out on the World Wild Web.

Instead i like to talk about important things, like encryption, open protocols, memetic mythology, how to build a meshed-network or which autotune is best for GNU+Linux. For that i can be found on [matrix](https://matrix.to/#/!YQcdVviFQNGYFMYrxD:matrix.org?via=matrix.org){: target="_blank" rel="noopener noreferrer"} and [telegram.](https://t.me/tosettosetto){: target="_blank" rel="noopener noreferrer"} Or by encrypted email to setto@basspistol.com. (I rarely respond to emails if they aren't [GPG](https://keyserver.ubuntu.com/pks/lookup?search=setto%40basspistol.com&amp;fingerprint=on&amp;op=index&amp;fbclid=IwAR14bD84366bC6e3XNV6nYkOy7DCcm5cBgM6Jmp74xWpdKdWsx3QaR77O1Y){: target="_blank" rel="noopener noreferrer"} signed.)

I like to sing in french because it's a dead language in Cyberspace. But it sounds lit and is quite functional for poetry. However, if I could I would probably sing in Haskell. At some point I will.

Hope you like my shit, or hate it for that matter\! Just make sure you share it\!