---
name: 徒 setto セット
position: 'Information Musician'
image: /siteicon.png
url_staff: 'https://setto.basspistol.com'
email: 'setto@basspistol.com'
gpg: 'https://keyserver.ubuntu.com/pks/lookup?search=setto%40basspistol.com&fingerprint=on&op=index'
blurb_markup: 'Set, Party, Gang'
---