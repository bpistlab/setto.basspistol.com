---
title: Pushing things differently
author: setto
date: 2020-09-17 00:00:00
description: >-
  Embracing the mainstream networks and reaching out through sponsored content,
  the underground is staging it's own isolation.
image: /images/posts/Screenshot_20200918_112312.jpg
video_path: /videos/37th--chamber.mp4
category: ramblings
link:
  url:
  buttontext:
check_this_if_cannot_be_bought: true
album:
stores:
  - name:
  - url:
  - icon:
  - download: false
---

"I post what's on my mind, therefore I am".&nbsp; The reflex is hard to avoid. The thumb swipes the screen by muscle-memory. A tiny but well oiled ritual. A digital routine. From the finger-tops we manipulate extensions that may not be attached to our bodies, yet at this point they are intrinsic part of ourselves. We want them, we need them and they keep needing replacement every each year or so. It's not so much the devices in themselves that suck us in, but the applications. What was meant to free us and facilitate our daily bullshit turned out to be alienating masters of complications.

Love/hate relationship doesn't quite cut it as a descriptor though: most of us probably realize something isn't too right with these tools, but how many can explain what, how and why? Society is slowly becoming unavailable to the disconnected: socializing, dating, creating, communicating, paying, learning… everything goes through the wireless wires. In and out of the very real data-centers made of concrete and silicon, that we call "clouds".

### But in the realm of the mind there is a choice: do i want to participate?

&nbsp;